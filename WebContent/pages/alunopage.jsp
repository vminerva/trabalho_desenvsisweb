<%@page import="br.com.dsw.control.DAO.AlunoDisciplinaDao"%>
<%@page import="br.com.dsw.model.AlunoDisciplina"%>
<%@page import="br.com.dsw.control.service.HomePage"%>
<%@page import="br.com.dsw.control.DAO.DisciplinaDao"%>
<%@page import="br.com.dsw.model.Disciplina"%>
<%@page import="java.util.List"%>
<%@page import="br.com.dsw.control.DAO.AlunoDao"%>
<%@page import="br.com.dsw.model.Aluno"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="template.jsp" />
<script src="../resources/js/validador.js"></script>

<div id="container">
	<div class="tab-pane" id="alunos">
		<div class="tab-pane active" id="notas">
			<div id="corpo">
				<form method="post" action="alunopage">
					<div style="float: left;">
					${message}
						<fieldset>
							<legend style="width: 364px;">Dados Aluno</legend>
							<table class="input-group input-group-sm">
								<tr>
									<td>Matricula:</td>
									<td><input type="text" name="matricula" onkeypress="return validador();" /></td>
									<td style="width: 22px;"></td>
								</tr>
								<tr>
									<td>Nome:</td>
									<td><input type="text" style="width: 290px;" name="nome" /></td>
								</tr>
								<tr>
									<td>Curso:</td>
									<td><select name="curso">
											<option>Sistemas de Informação</option>
											<option>Analise Desenv. de Sistemas</option>
									</select></td>
								</tr>
								<tr>
									<td>Disciplina:</td>
									<td><select name="disciplina" style="width: 289px;">
											<%
												DisciplinaDao disciplinaDao = new DisciplinaDao();
												List<Disciplina> disciplinas = disciplinaDao.listar();
												for (Disciplina d : disciplinas) {
											%>
											<option>
												<%=d.getDescricao()%>
											</option>
											<%
												}
											%>
									</select></td>
								</tr>
							</table>
							<input type="submit" value="salvar" class="btn-primary btn" /> <a
								href="homepage.jsp">
								<button type="button" class="btn-primary btn">Cancelar</button>
							</a> 
						</fieldset>
					</div>
				</form>
				<div style="float: right;">
<!-- 					<div -->
<!-- 						style="overflow: auto; width: 100%; height: 200px; margin-top: 20px;">  -->
<!-- 						<table border="1px solid" class="table table-striped" -->
<!-- 							style="width: 202px;"> -->
<!-- 							<tr style="font: bold;"> -->
<!-- 								<td>Descrição</td> -->
<!-- 								<td>Notas</td> -->
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td></td> -->
<!-- 								<td></td> -->
<!-- 							</tr> -->
<!-- 						</table> -->
<!-- 					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="rodape.jsp" />
