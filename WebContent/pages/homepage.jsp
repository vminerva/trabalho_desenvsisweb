<%@page import="java.math.RoundingMode"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.dsw.control.service.CalculoMediaPageModal"%>
<%@page import="br.com.dsw.model.Nota"%>
<%@page import="br.com.dsw.control.DAO.NotaDao"%>
<%@page import="br.com.dsw.control.DAO.AlunoDisciplinaDao"%>
<%@page import="br.com.dsw.model.AlunoDisciplina"%>
<%@page import="br.com.dsw.control.service.HomePage"%>
<%@page import="br.com.dsw.control.DAO.DisciplinaDao"%>
<%@page import="br.com.dsw.model.Disciplina"%>
<%@page import="java.util.List"%>
<%@page import="br.com.dsw.control.DAO.AlunoDao"%>
<%@page import="br.com.dsw.model.Aluno"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="template.jsp" />
<script src="../resources/js/validador.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(exp).keyup(function() {
        this.value = this.value.toString().toUpperCase();
    });
});
</script>
<div id="container">
	<div class="tab-content">
		<div class="tab-pane active" id="home">
			<div id="corpo">
				<form method="post" action="homepage">
					${message}
					<fieldset style="border: 1px solid">
						<legend>Pesquisar Aluno(s)</legend>
						<table class="input-group input-group-sm">
							<tr>
								<td>Matricula:</td>
								<td><input type="text" name="matriculaBusca"
									class="form-control" onkeypress="return validador();" /></td>
							</tr>
							<tr>
								<td>Nome:</td>
								<td><input type="text" name="nomeBusca"
									style="width: 500px;" /></td>
							</tr>
							<tr>
								<td>Disciplina:</td>
								<td><select name="disciplinaBusca" style="width: 289px;">
										<option value="<%=""%>">Selecione</option>

										<%
											Disciplina disciplina = new Disciplina();
											DisciplinaDao disciplinaDao = new DisciplinaDao();
											List<Disciplina> disciplinas = disciplinaDao.listar();
											for (Disciplina d : disciplinas) {
										%>
										<option value="<%=d.getIdDisciplina()%>">
											<%=d.getDescricao()%>
										</option>
										<%
											}
										%>
								</select>
								<td><input type="submit" value="buscar"
									class="btn-primary btn" style="width: 150px;" /></td>
							</tr>
						</table>
					</fieldset>
					<div
						style="overflow: auto; width: 100%; height: 200px; margin-top: 20px;">
						<table border="1px solid" class="table table-striped">
							<tr style="font: bold;">
								<td>Matrícula</td>
								<td>Nome</td>
								<td>Disciplina</td>
								<td>Média</td>
								<td>Nota 1</td>
								<td>Nota 2</td>
								<td>Nota 3</td>
							</tr>
							<%
								AlunoDisciplinaDao alunoDao = new AlunoDisciplinaDao();

								List<AlunoDisciplina> list = new ArrayList<AlunoDisciplina>();
								list = (List<AlunoDisciplina>) request.getAttribute("list");

								List<AlunoDisciplina> alunos = null;

								List<Nota> notasAluno = null;

								if (list != null && list.size() > 0) {

									alunos = list;

								}

								if (alunos == null) {

									alunos = alunoDao.listar();

								}

								for (AlunoDisciplina a : alunos) {
									notasAluno = a.getAlunoId().getNotas();
							%>
							<tr>
								<td><%=a.getAlunoId().getMatricula()%></td>
								<td><%=a.getAlunoId().getNome()%></td>
								<td><%=a.getDisciplinaId().getDescricao()%></td>
								<% if(a.getMedia() != null ){
									BigDecimal bg = new BigDecimal(a.getMedia()).setScale(2, RoundingMode.HALF_UP);
									a.setMedia(bg.doubleValue());
								%>								
								<td><%=a.getMedia()%></td>
								<%	}else{	%>
								<td><%=a.getMedia() %></td>
								<% 	}
									for (Nota controleAluno : notasAluno) {
											if (notasAluno.size() > 3) {
												for (int i = 0; i < 3; i++) {
								%>
								<td><%=notasAluno.get(i).getNota()%></td>
								<%
									}
												break;
											} else {
								%>
								<td><%=controleAluno.getNota()%></td>
								<%
									}
										}
								%>
								<%
									}
								%>
							</tr>
						</table>
					</div>
				</form>
				<!-- Butao de acionar o  modal -->
				<button class="btn btn-primary btn-lg" data-toggle="modal"
					data-target="#myModal">Calculo Média</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="container">
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span>
					</button>
					<h2 class="modal-title" id="myModalLabel">Calculo da Média</h2>
					<div class="modal-body">
						<form method="post" action="CalculoMediaPageModal">
							<h4>Expressão:</h4>
							<input type="text" name="expressao" id="exp" placeholder="ex.:(AV1+AV2+AV3)/3">
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Sair</button>
								<button type="submit" class="btn btn-primary">Salvar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="rodape.jsp" />
