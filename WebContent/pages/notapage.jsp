<%@page import="br.com.dsw.control.DAO.AlunoDisciplinaDao"%>
<%@page import="br.com.dsw.model.AlunoDisciplina"%>
<%@page import="br.com.dsw.control.service.HomePage"%>
<%@page import="br.com.dsw.control.DAO.DisciplinaDao"%>
<%@page import="br.com.dsw.model.Disciplina"%>
<%@page import="java.util.List"%>
<%@page import="br.com.dsw.control.DAO.AlunoDao"%>
<%@page import="br.com.dsw.model.Aluno"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="template.jsp" />
<script src="../resources/js/validador.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(tipo).keyup(function() {
        this.value = this.value.toString().toUpperCase();
    });
});
</script>
<div id="container">
	<div class="tab-pane" id="notas">
		<div class="tab-pane active" id="notas">
			<div id="corpo">
				<form method="post" action="notaspage">
				${message}
					<fieldset>
						<legend style="width: 364px;">Dados Aluno</legend>
						<table class="input-group input-group-sm">
							<tr>
								<td>Matricula:</td>
								<td><input type="text" name="matriculaAluno" onkeypress="return validador();" /></td>
								<td style="width: 22px;"></td>
								<td>Nome:</td>
								<td><input type="text" name="nomeAluno"
									style="width: 290px;" /></td>
							</tr>
						</table>
						<fieldset>
							<legend style="width: 364x;">Notas</legend>
							<div style="float: left; padding-left: 20px;">
								<table class="input-group input-group-sm">
									<tr>
										<td>Nota:</td>
										<td><input type="text" name="notaAluno" onkeypress="return validador();"
											style="width: 40px;" /></td>
									</tr>
									<tr>
										<td>Tipo:</td>
										<td><input type="text" id="tipo" name="tipoNota" placeholder="ex.: AV1">
									</tr>
<!-- 									<tr> -->
<!-- 										<td>Peso:</td> -->
<!-- 										<td><select name="pesoNota"> -->
<!-- 												<option>Peso 1</option> -->
<!-- 												<option>Peso 2</option> -->
<!-- 												<option>Peso 3</option> -->
<!-- 										</select></td> -->
<!-- 									</tr> -->
									<tr>
										<td>Disciplina:</td>
										<td><select name="notaDisciplina" style="width: 289px;">
												<%
													DisciplinaDao disciplinaDao = new DisciplinaDao();
													List<Disciplina> disciplinas = disciplinaDao.listar();
													for (Disciplina d : disciplinas) {
												%>
												<option>
													<%=d.getDescricao()%>
												</option>
												<%
													}
												%>
										</select></td>
									</tr>
								</table>
							</div>
							<div style="float: right; padding-right: 100px;">
<!-- 								<table class="input-group input-group-sm"> -->
<!-- 									<tr> -->
<!-- 										<td>Trabalho:</td> -->
<!-- 										<td><input type="text" name="notaTrabalho" style="width: 40px;" /></td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<td>Exercicio Complementar:</td> -->
<!-- 										<td><input type="text" style="width: 40px;" /></td> -->
<!-- 									</tr> -->
<!-- 								</table> -->

							</div>
						</fieldset>
					</fieldset>
					<br>
					<div class="botoes">
						<input type="submit" value="salvar" class="btn-primary btn" />
						<a	href="homepage.jsp"> <button type="button" class="btn-primary btn">Cancelar</button></a>
					</div> 
				</form>
			</div>
		</div>
	</div>
</div>
<c:import url="rodape.jsp" />
