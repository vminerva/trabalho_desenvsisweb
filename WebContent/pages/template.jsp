<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- IMPORT BOOTSTRAP E CSS -->
<link rel="stylesheet" type="text/css"
	href="../resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="../resources/css/estilo.css">
<script type="text/javascript" charset="utf-8"
	src="../resources/js/jquery.leanModal.min.js"></script>
<script src="../resources/js/jquery-1.11.1.min.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<title>Sistema para controle de notas de alunos</title>
</head>
<body>
	<div id="container">
		<div id="titulo">
			<p class="page-header">Sistema de Controle de Notas de Alunos</p>
		</div>
		<div id="barra_menu">
			<ul class="nav nav-tabs" role="tablist" id="myTab">
				<li><a href="homepage.jsp">Home</a></li>
				<li><a href="notapage.jsp">Notas</a></li>
				<li><a href="alunopage.jsp">Alunos</a></li>
				<li><input type="text" class="search-query"
					placeholder="Pesquisar" /></li>
			</ul>
		</div>
	</div>
</body>
</html>