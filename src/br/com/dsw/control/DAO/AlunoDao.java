package br.com.dsw.control.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import br.com.dsw.model.Aluno;

public class AlunoDao extends Dao{

	private static final long serialVersionUID = -7907741456468748527L;

	private Aluno aluno;
	
	@Override
	public void salvar(Object objeto) {
		aluno = (Aluno) objeto;
		save(aluno);
	}

	@Override
	public void excluir(Object objeto) {
		aluno = (Aluno) objeto;
		delete(aluno);
	}

	@Override
	public Object consultar(Integer id) {
		aluno = (Aluno) consult(new Aluno(), id);
		return aluno;
	}

	@Override
	public void editar(Object objeto) {
		aluno = (Aluno) objeto;
		edit(aluno);
	}

	@Override
	public <T> List<T> listar() {
		return findAll("Aluno");
	}
	
	public Object consultaMatricula(String matricula){
		aluno = (Aluno) getGerenciaEntidades().createQuery("FROM Aluno WHERE matricula = "+matricula);
		return aluno;
	}
}
