package br.com.dsw.control.DAO;

import java.util.List;

import javax.persistence.Query;

import br.com.dsw.model.AlunoDisciplina;

public class AlunoDisciplinaDao extends Dao {

	private static final long serialVersionUID = 2701170608231990396L;
	AlunoDisciplina alunoDisciplina;

	@Override
	public void salvar(Object objeto) {
		alunoDisciplina = (AlunoDisciplina) objeto;
		save(alunoDisciplina);
	}

	@Override
	public void excluir(Object objeto) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object consultar(Integer id) {
		alunoDisciplina = (AlunoDisciplina) consult(new AlunoDisciplina(), id);
		return alunoDisciplina;
	}

	@Override
	public void editar(Object objeto) {
		// TODO Auto-generated method stub
	}

	@Override
	public <T> List<T> listar() {
		return findAll("AlunoDisciplina");
	}

	@SuppressWarnings("unchecked")
	public List<AlunoDisciplina> filtro(String nome, Long matricula,
			Integer disciplina) {

		Query query = null;

		StringBuilder builder = new StringBuilder();
		builder.append("from AlunoDisciplina a where 1=1");

		if (nome != null && !(nome.trim().equals(""))) {
			builder.append(" and a.alunoId.nome LIKE '%");
			builder.append(nome);
			builder.append("%'");
		}

		if (matricula != null && matricula > 0) {
			builder.append(" and a.alunoId.matricula=");
			builder.append(matricula);
		}

		if (disciplina != null && disciplina > 0) {
			builder.append(" and a.disciplinaId=");
			builder.append(disciplina);
		}

		query = getGerenciaEntidades().createQuery(builder.toString());

		return query.getResultList();

	}

}
