package br.com.dsw.control.DAO;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public abstract class Dao implements Serializable {

	private static final long serialVersionUID = -3186109970233989876L;

	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("ControleNotas");
	private static EntityManager gerenciaEntidades = factory.createEntityManager();

	protected final static void save(Object objeto) {
		gerenciaEntidades.getTransaction().begin();
		gerenciaEntidades.persist(objeto);
		gerenciaEntidades.getTransaction().commit();
	}

	protected final static void edit(Object objeto) {
		gerenciaEntidades.getTransaction().begin();
		gerenciaEntidades.merge(objeto);
		gerenciaEntidades.getTransaction().commit();
	}

	protected final static void delete(Object objeto) {
		gerenciaEntidades.getTransaction().begin();
		gerenciaEntidades.remove(objeto);
		gerenciaEntidades.getTransaction().commit();
	}

	protected final static Object consult(Object objeto, Integer id) {
		return gerenciaEntidades.find(objeto.getClass(), id);
	}

	@SuppressWarnings("unchecked")
	protected final <T> List<T> findAll(String parametro) {
		return gerenciaEntidades.createQuery("from " + parametro)
				.getResultList();
	}

	public EntityManager getGerenciaEntidades() {
		return gerenciaEntidades;
	}

	public abstract void salvar(Object objeto);

	public abstract void excluir(Object objeto);

	public abstract Object consultar(Integer id);

	public abstract void editar(Object objeto);

	public abstract <T> List<T> listar();
}
