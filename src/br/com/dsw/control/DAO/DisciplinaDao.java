package br.com.dsw.control.DAO;

import java.util.List;

import br.com.dsw.model.Disciplina;

public class DisciplinaDao extends Dao{

	private static final long serialVersionUID = -7905410194899364211L;
	
	Disciplina disciplina;
	
	@Override
	public void salvar(Object objeto) {}

	@Override
	public void excluir(Object objeto) {}

	@Override
	public Object consultar(Integer id) {return null;}

	@Override
	public void editar(Object objeto) {}

	@Override
	public <T> List<T> listar() {
		return findAll("Disciplina");
	}

}
