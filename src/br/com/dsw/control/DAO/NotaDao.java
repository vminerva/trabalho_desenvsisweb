package br.com.dsw.control.DAO;

import java.util.List;

import br.com.dsw.model.Nota;

public class NotaDao extends Dao{

	private static final long serialVersionUID = -7889136958744649374L;

	@Override
	public void salvar(Object objeto) {
		Nota nota = (Nota) objeto;
		save(nota);
	}

	@Override
	public void excluir(Object objeto) {
	}

	@Override
	public Object consultar(Integer id) {
		return null;
	}

	@Override
	public void editar(Object objeto) {
	}

	@Override
	public <T> List<T> listar() {
		return findAll("Nota");
	}
	
	public <T> List<T> listarNotasAluno(Integer id){
		return getGerenciaEntidades().createQuery("FROM Nota WHERE alunoId_idAluno = "+id).getResultList();
		
	}
}
