package br.com.dsw.control.service;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.RollbackException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.research.ws.wadl.Request;

import br.com.dsw.control.DAO.AlunoDao;
import br.com.dsw.control.DAO.AlunoDisciplinaDao;
import br.com.dsw.control.DAO.DisciplinaDao;
import br.com.dsw.model.Aluno;
import br.com.dsw.model.AlunoDisciplina;
import br.com.dsw.model.Disciplina;
import br.com.dsw.model.Nota;
import br.com.dsw.service.ExpressaoMatematica;

@WebServlet("/pages/alunopage")
public class AlunoPage extends HttpServlet {
	private static final long serialVersionUID = -7101321895405622449L;
	private Map<String, Double> mapNotas;

	public AlunoPage() {
	}

	public AlunoPage(String expressao) throws NullPointerException {
		AlunoDisciplinaDao alunoDao = new AlunoDisciplinaDao();
		List<AlunoDisciplina> alunos = alunoDao.listar();
		List<Nota> notasAluno = null;
		for (AlunoDisciplina alunoDisciplina : alunos) {
			notasAluno = alunoDisciplina.getAlunoId().getNotas();
			mapNotas = new HashMap<String, Double>();
			if (!notasAluno.isEmpty() && mapNotas.size() < 3) {
				mapNotas.put("AV3", 0D);
				mapNotas.put("TRABALHO", 0d);
			} else if (mapNotas.isEmpty()) {
				break;
			}
			for (Nota nota : notasAluno) { // sai do 1 la�o
				if (notasAluno.isEmpty()) {
					break;
				}
				mapNotas.put(nota.getTipo(), nota.getNota());
			}
			String expressaoTratada = trataExpressao(expressao);
			Double calcMedia = new ExpressaoMatematica(expressaoTratada)
					.resolve();
			alunoDisciplina.setMedia(calcMedia);
			alunoDao.salvar(alunoDisciplina);
		}
	}

	protected void service(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException,NullPointerException {
		request.setCharacterEncoding("UTF-8");
		
		try {
			String matricula = request.getParameter("matricula");
			String nome = request.getParameter("nome");
			String curso = request.getParameter("curso");
			String disciplinaNome = request.getParameter("disciplina");
			
			if(verificaNulos(matricula, nome)){
				request.setAttribute("message", "H� campos nulos,preencha!");
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/pages/alunopage.jsp");
				dispatcher.forward(request, resp);
			}
			else{
			
				Aluno aluno = new Aluno();
				aluno.setMatricula(matricula);
				aluno.setNome(nome);
				aluno.setCurso(curso);
				aluno.setNotas(new ArrayList<Nota>());
				AlunoDao alunoDao = new AlunoDao();
				alunoDao.salvar(aluno);

				Disciplina disciplina = atribuiDisciplinaAluno(disciplinaNome);

				salvaAlunoComDisciplina(aluno, disciplina);

				request.setAttribute("message", "* Cadastro efetuado com sucesso!");
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/pages/homepage.jsp");
				dispatcher.forward(request, resp);
			}
		}
		 catch (Exception e) {
			request.setAttribute("message", "* Erro ao cadastrar!");
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/pages/homepage.jsp");
			dispatcher.forward(request, resp);
		}
	}

	private Disciplina atribuiDisciplinaAluno(String disciplinaNome) {
		Disciplina disciplina = new Disciplina();
		DisciplinaDao disciplinaDao = new DisciplinaDao();
		List<Disciplina> disciplinas = disciplinaDao.listar();
		for (Disciplina disc : disciplinas) {
			System.out.println(disc.getDescricao());
			if (disciplinaNome.equals(disc.getDescricao())) {
				disciplina.setIdDisciplina(disc.getIdDisciplina());
				disciplina.setDescricao(disc.getDescricao());
			}
		}
		return disciplina;
	}

	private void salvaAlunoComDisciplina(Aluno aluno, Disciplina disciplina) {
		AlunoDisciplina alunoDisciplina = new AlunoDisciplina();
		alunoDisciplina.setAlunoId(aluno);
		alunoDisciplina.setDisciplinaId(disciplina);
		AlunoDisciplinaDao controleAlunoDao = new AlunoDisciplinaDao();
		controleAlunoDao.salvar(alunoDisciplina);
	}

	public Double recuperaValor(String descricao) {
		return mapNotas.get(descricao);
	}

	private String trataExpressao(String expressao) {
		expressao = trataExpressaoTrabalho(expressao);
		Pattern padrao = Pattern.compile("AV[1-9]");
		Matcher matcher = padrao.matcher(expressao);
		
		while (matcher.find()) {
			expressao = expressao.replaceFirst(matcher.group(),
					recuperaValor(matcher.group()).toString());
		}
		
		return expressao;
	}
	
	public String trataExpressaoTrabalho(String expressao){
		Pattern padrao = Pattern.compile("TRABALHO");
		Matcher matcher = padrao.matcher(expressao);
		
		while (matcher.find()) {
			expressao = expressao.replaceFirst(matcher.group(),
					recuperaValor(matcher.group()).toString());
		}
		return expressao;
	}
	
	private boolean verificaNulos(String matricula, String nome){
		boolean verifica = false;
		
		if(matricula.equals("")|| nome.equals("")){
			verifica= true;
			
		}
		return verifica;
	}
	private void verificarNumeros(KeyEvent ev){
		
		String carac ="0987654321";
		if(!carac.contains(ev.getKeyChar()+"")){
			ev.consume();
			
		}
		
	}
	
		
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
		service(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doPost(req, resp);
		service(req, resp);
	}
}
