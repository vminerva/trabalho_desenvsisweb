package br.com.dsw.control.service;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/pages/CalculoMediaPageModal")
public class CalculoMediaPageModal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String expressao = req.getParameter("expressao");
		
		try{
			new AlunoPage(expressao);
		}catch(NullPointerException e){
			req.setAttribute("message", "* Erro na express�o!");
			RequestDispatcher dispatcher = req
					.getRequestDispatcher("/pages/homepage.jsp");
			dispatcher.forward(req, resp);
		}
		
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/pages/homepage.jsp");
		dispatcher.forward(req, resp);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	}

	
	

}
