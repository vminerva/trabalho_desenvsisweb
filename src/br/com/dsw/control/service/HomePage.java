package br.com.dsw.control.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dsw.control.DAO.AlunoDisciplinaDao;
import br.com.dsw.model.AlunoDisciplina;

/**
 * Servlet implementation class HomePage
 */
@WebServlet("/pages/homepage")
public class HomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Boolean isBuscaFiltrada = false;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		try {

			String nome = req.getParameter("nomeBusca");
			Long matricula = req.getParameter("matriculaBusca").equals("") ? null
					: Long.parseLong(req.getParameter("matriculaBusca"));
			Integer disciplina = (req.getParameter("disciplinaBusca").equals(
					"Selecione") || req.getParameter("disciplinaBusca").equals("")) ? null : Integer.parseInt(req
					.getParameter("disciplinaBusca"));

			if (verificaNulos(matricula, nome, disciplina)) {

				List<AlunoDisciplina> list = new ArrayList<AlunoDisciplina>();
				AlunoDisciplinaDao controleAlunoDao = new AlunoDisciplinaDao();

				list = controleAlunoDao.filtro(nome, matricula, disciplina);

				if (list.size() > 0) {
					req.setAttribute("list", list);
					req.setAttribute("message",
							"* Pesquisa Realizada com sucesso!!");
				} else {
					req.setAttribute("message", "* Nenhum valor referente!!");
				}

			} else {

				req.setAttribute("message",
						"* Digite pelo menos algum valor valido em um dos campos");

			}

		} catch (Exception exception) {

			exception.printStackTrace();
			req.setAttribute("message", "* Erro durante a pesquisa do filtro");
		}

		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/pages/homepage.jsp");
		dispatcher.forward(req, resp);

	}

	private boolean verificaNulos(Long matricula, String nome,
			Integer disciplina) {
		// TODO Auto-generated method stub

		boolean verifica = true;

		if ((matricula == null || matricula < 0)
				&& (nome == null || nome.trim().equals(""))
				&& (disciplina == null)) {
			verifica = false;
		}
		return verifica;
	}
}
