package br.com.dsw.control.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dsw.control.DAO.AlunoDao;
import br.com.dsw.control.DAO.DisciplinaDao;
import br.com.dsw.control.DAO.NotaDao;
import br.com.dsw.model.Aluno;
import br.com.dsw.model.Disciplina;
import br.com.dsw.model.Nota;
/**
 * Servlet implementation class NotasPage
 */
@WebServlet("/pages/notaspage")
public class NotasPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Disciplina getDisciplina;
	private Aluno getAluno;
	
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		try{
		String matriculaAluno = request.getParameter("matriculaAluno");
		String notaAluno = request.getParameter("notaAluno");
		String notaTipo = request.getParameter("tipoNota");
		String disciplinaNome = request.getParameter("notaDisciplina");

		if(verificaCampos(matriculaAluno, notaAluno, notaTipo)){
			request.setAttribute("message", "H� campos vazios,preencha-os!");
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/pages/notapage.jsp");
			dispatcher.forward(request, response);
		}else{
		
		verificaDisciplinaAlunoNota(disciplinaNome);
		
		verificaMatriculaAluno(matriculaAluno);
		
		atribuiNotaAluno(notaAluno, notaTipo);
		
		request.setAttribute("message", "Cadastro efetuado com sucesso!");
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/pages/homepage.jsp");
		dispatcher.forward(request, response);
		}
		}catch(Exception e){
			request.setAttribute("message","Erro ao cadastrar nota!");
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/pages/homepage.jsp");
					dispatcher.forward(request, response);
		}
	}
	
	private void verificaDisciplinaAlunoNota(String disciplinaNome) {
		DisciplinaDao disciplinaDao = new DisciplinaDao();
		List<Disciplina> disciplinas = disciplinaDao.listar();
		for (Disciplina disc : disciplinas) {
			if (disciplinaNome.equals(disc.getDescricao())) {
				getDisciplina = disc;
				break;
			}
		}
	}

	private void verificaMatriculaAluno(String matriculaAluno) {
		AlunoDao alunoDao = new AlunoDao();
		List<Aluno> listaAlunos = alunoDao.listar();
		for (Aluno aluno : listaAlunos) {
			if (matriculaAluno.equals(aluno.getMatricula())) {
				getAluno = aluno;
				break;
			}
		}
	}

	private void atribuiNotaAluno(String notaAluno, String notaTipo) {
		Nota nota = new Nota();
		nota.setNota(Double.parseDouble(notaAluno));
		nota.setTipo(notaTipo);
		nota.setAlunoId(getAluno);
		nota.setDisciplinaId(getDisciplina);
		NotaDao notaDao = new NotaDao();
		notaDao.salvar(nota);
	
		List<Nota> setNotasAluno = notaDao.listarNotasAluno(getAluno.getIdAluno());
		getAluno.setNotas(setNotasAluno);
	}
	
	private boolean verificaCampos(String matriculaAluno,String notaAluno,String notaTipo){
		boolean verifica = false;
		
		if(matriculaAluno.equals("")|| notaAluno.equals("")|| notaTipo.equals("")){
			verifica =true;
		}
		return verifica;
	}
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	}
}
