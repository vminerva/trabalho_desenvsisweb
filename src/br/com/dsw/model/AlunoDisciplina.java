package br.com.dsw.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AlunoDisciplina {

	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	private Aluno alunoId;

	@ManyToOne
	private Disciplina disciplinaId;

	private Double media;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Aluno getAlunoId() {
		return alunoId;
	}

	public void setAlunoId(Aluno alunoId) {
		this.alunoId = alunoId;
	}

	public Disciplina getDisciplinaId() {
		return disciplinaId;
	}

	public void setDisciplinaId(Disciplina disciplinaId) {
		this.disciplinaId = disciplinaId;
	}

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	
}
