package br.com.dsw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.dsw.model.interfaces.Expressao;

@Entity
public class Nota implements Serializable, Expressao {

	private static final long serialVersionUID = -7258084201986329097L;

	@Id
	@GeneratedValue
	private Integer idNota;

	private Double nota;
	
	private String tipo;
	
	@ManyToOne
	private Aluno alunoId;
	
	@ManyToOne
	private Disciplina disciplinaId;
	
	@Override
	public Double avalia() {
		return null;
	}

	public Integer getIdNota() {
		return idNota;
	}

	public void setIdNota(Integer id) {
		this.idNota = id;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Aluno getAlunoId() {
		return alunoId;
	}

	public void setAlunoId(Aluno alunoId) {
		this.alunoId = alunoId;
	}

	public Disciplina getDisciplinaId() {
		return disciplinaId;
	}

	public void setDisciplinaId(Disciplina disciplinaId) {
		this.disciplinaId = disciplinaId;
	}

}
