package br.com.dsw.model.enuns;

public enum Operador {

    SOMA("+", 0) {
        @Override
        public Double resolve(Double ladoEsquerdo, Double ladoDireito) {
            return ladoEsquerdo + ladoDireito;
        }
    },
    SUBTRACAO("-", 0) {
        public Double resolve(Double ladoEsquerdo, Double ladoDireito) {
            return ladoEsquerdo - ladoDireito;
        }
    },
    MULTIPLICACAO("*", 10) {
        public Double resolve(Double ladoEsquerdo, Double ladoDireito) {
            return ladoEsquerdo * ladoDireito;
        }
    },
    DIVISAO("/", 10) {
        public Double resolve(Double ladoEsquerdo, Double ladoDireito) {
            return ladoEsquerdo / ladoDireito;
        }
    };

    private String operador;
    private int prioridade;

    private Operador(String operador, int prioridade) {
        this.operador = operador;
        this.prioridade = prioridade;
    }

    /**
     * Responável por efetuar a operação em cima das parcelas informadas
     *
     * @param ladoEsquerdo O valor do lado esquerdo da operação.
     * @param ladoDireito  O valor do lado direito da operação.
     * @return O resultado da operação.
     */
    public abstract Double resolve(Double ladoEsquerdo, Double ladoDireito);

    /**
     * Responsável por recuperar o operador como String.
     *
     * @return A String que representa o operador em questão.
     */
    public String getOperador() {
        return operador;
    }

    /**
     * Responsável por recuperar a prioridade do operador em questão.
     *
     * @return O valor da prioridade do operador.
     */
    public int getPrioridade() {
        return prioridade;
    }

    /**
     * Responsável por verificar qual a precedencia do operador em questão
     * com relação a outro operador informado.
     *
     * @param operador O outro operador a ser compadado.
     * @return Verdadeiro caso a precedencia do operador corrente seja maior
     * ou igual ao operador informado.
     */
    public boolean temPrecedenciaMaiorOuIgualAo(Operador operador) {
        return prioridade >= operador.getPrioridade();
    }
}
