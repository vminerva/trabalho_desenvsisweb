package br.com.dsw.service;

public interface Expressao {
    public Double resolve();

    public Double getValor(String exp);
}
