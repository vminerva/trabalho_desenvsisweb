package br.com.dsw.service;

import br.com.dsw.model.enuns.Operador;

public class ExpressaoMatematica implements Expressao {
    private Operador operador;
    private ExpressaoMatematica ladoEsquerdo;
    private ExpressaoMatematica ladoDireito;
    private Double valor;

    /**
     * Responsável por construir cada nó da arvore recursivamente a partir
     * da expressão informada pelo professor na web.
     *
     * @param exp A expressão a ser avaliada.
     */
    public ExpressaoMatematica(String exp) {
        exp = removeParenteses(exp);

        if (!temTodosOsParenteses(exp)) {
            throw new IllegalArgumentException("Número errado de parenteses na expressão: " + exp);
        }

        valor = getValor(exp);

        int achouParentese = 0;
        int posicaoDoOperador = 0;

        // Para cada caractere da expressão...
        for (int i = 0; i < exp.length(); i++) {
            if (exp.charAt(i) == '(') {
                achouParentese++;
            } else if (exp.charAt(i) == ')') {
                achouParentese--;
            } else {
                // Do parentese aberto, até o seu correspondente fechado..
                if (achouParentese == 0) {
                    Operador outroOperador = extraiOperador(exp, i);
                    if (outroOperador != null) {
                        if (this.operador == null || outroOperador.temPrecedenciaMaiorOuIgualAo(operador)) {
                            this.operador = outroOperador;
                            posicaoDoOperador = i;
                        }
                    }
                }
            }
        }

        if (operador != null) {
            if (posicaoDoOperador > 0) {
                ladoEsquerdo = new ExpressaoMatematica(exp.substring(0, posicaoDoOperador));
                ladoDireito = new ExpressaoMatematica(exp.substring(posicaoDoOperador + 1));
            }
        }
    }

    /**
     * Responsável por "resolver" a expressão matemática em si. É aqui que a montagem
     * da arvore se inicia. Caso a expressão contenha nós, ela vai sendo montada.
     *
     * @return O valor da expressão matemática esperado.
     */
    @Override
    public Double resolve() {
        try {
            if (temOperador() && temFilhos()) {
                valor = operador.resolve(ladoEsquerdo.resolve(), ladoDireito.resolve());
            }
            return valor;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Responsável por recuperar o valor da expressão após ser avaliada.
     *
     * @param exp A expressão a ser avaliada.
     * @return O valor da expressão, ou nulo caso ainda não seja o fim da arvores.
     */
    @Override
    public Double getValor(String exp) {
        try {
            return Double.parseDouble(exp);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Responsável por verificar se a expressão tem filhos ou não.
     * Ou seja, se ainda tem nós abaixo dele a serem avaliados.
     *
     * @return Verdadeiro caso tenha, falso caso contrário.
     */
    private boolean temFilhos() {
        return ladoEsquerdo != null || ladoDireito != null;
    }

    /**
     * Responsável por verificar se a expressão tem algum operador ou não.
     *
     * @return Verdadeiro caso tenha, falso caso contrário.
     */
    private boolean temOperador() {
        return operador != null;
    }

    /**
     * Responsável por extrair o operador da expressão corrente.
     *
     * @param exp               A expressão a ser avaliada.
     * @param posicaoDoOperador A posição na expressão onde o operador se encontra.
     * @return O operador da expressão, ou nulo caso seja apenas um número.
     */
    private Operador extraiOperador(String exp, int posicaoDoOperador) {
        Operador[] operadores = Operador.values();
        String ladoDireito = extraiLadoDireito(exp.substring(posicaoDoOperador));
        for (Operador operador : operadores) {
            if (ladoDireito.startsWith(operador.getOperador())) {
                return operador;
            }
        }

        return null;
    }

    /**
     * Responsável por extrair o próximo lado (ou seja, o direito)
     * da expressão que está sendo interada, para verificar qual o operador
     * daquela expressão.
     *
     * @param exp A expressão a ser avaliada.
     * @return Uma sub-expressão a ser avaliada na iteração de montagem da arvore.
     */
    private String extraiLadoDireito(String exp) {
        for (int i = 1; i < exp.length(); i++) {
            char c = exp.charAt(i);
            if ((c > 'z' || c < 'a') && (c > '9' || c < '0')) {
                return exp.substring(0, i);
            }
        }
        return exp;
    }

    /**
     * Responsável por verificar se a expressão fornecida contém
     * o número certo de parenteses. Ou seja, ela verifica se todos os
     * parenteses abertos, foram fechados.
     *
     * @param exp A expressão a ser avaliada.
     * @return verdadeiro se a expressão contiver todos os parenteses ou falso caso constrário.
     */
    private boolean temTodosOsParenteses(String exp) {
        int parenteses = 0;
        for (int i = 0; i < exp.length(); i++) {
            if (exp.charAt(i) == '(' && parenteses >= 0) {
                parenteses++;
            } else if (exp.charAt(i) == ')') {
                parenteses--;
            }
        }

        return parenteses == 0;
    }

    /**
     * Responsável por remover os parênteses a casa interação pela expressão.
     *
     * @param exp A expressão a ser removida os parênteses.
     * @return Uma nova expressão, porém, sem os parênteses para aquela iteração.
     */
    private String removeParenteses(String exp) {
        if (exp.length() > 2 && exp.startsWith("(") && exp.endsWith(")") && temTodosOsParenteses(exp.substring(1, exp.length() - 1))) {
            exp = exp.substring(1, exp.length() - 1);
        }
        return exp;
    }
}
